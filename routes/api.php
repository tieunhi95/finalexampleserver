<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('user', 'UserController');
Route::get('symptom/register-date/{registerDate}', 'SymptomController@registerDate');
Route::get('symptom/get-by-month/{registerDate}', 'SymptomController@getByMonth');
Route::get('symptom/get-by-7-day/{registerDate}', 'SymptomController@getBy7Day');
Route::get('message/{id?}', 'MessageController@index');
Route::post('message', 'MessageController@store');
Route::put('set-medicine/{id}', 'UserController@setMedicine');
Route::resource('symptom', 'SymptomController');
Route::resource('medicine', 'MedicineController');
Route::resource('skincare', 'SkincareController');
Route::post('user/{id}', 'UserController@update')->name('update');
Route::post('login', 'UserController@login')->name('login');
Route::post('logout', 'UserController@logout')->name('logout');
