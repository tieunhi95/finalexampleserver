<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Auth extends Model
{
    //

    protected $table = 'auths';
    public $timestamps = false;
}
