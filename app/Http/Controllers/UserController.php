<?php

namespace App\Http\Controllers;

use App\Auth;
use App\Http\Requests\UserRequest;
use App\Library\StringHelper;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    protected $request;
    protected $user;
    protected $auth;

    /**
     *
     * @param Request $request
     * @param User $user
     * @param Auth $auth
     */
    public function __construct(Request $request, User $user, Auth $auth)
    {
        $this->request = $request;
        $this->user = $user;
        $this->auth = $auth;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = $this->user->all();
        return response()->json(['data' => $user,
            'status' => Response::HTTP_OK]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = $this->request->all();
            $validator = Validator::make($request->all(), [
                'name' => 'max:127',
                'email' => 'required|email|max:127|unique:users,email',
                'password' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json([
                    'status' => 0,
                    'error' => $validator->errors()], 401);
            }
            if (!empty($data['file'])) {
                $extension = $data['file']->getClientOriginalExtension(); // getting image extension
                $filename = time() . '.' . $extension;
                $data['file']->move('images/users/', $filename);
                $data['picture'] = $filename;
                unset($data['file']);
            }
            $userId = $this->user->insertGetId($data);
            $user = $this->user->find($userId);
            return response()->json(['data' => $user,
                'status' => 1]);
        } catch (Exception $exception) {
            return response()->json(['status' => 0]);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $this->request->all();
            if (empty($data['token'])) {
                throw new Exception('Token required', 1);
            }
            $checkToken = $this->checkToken($data['token']);
            if ($checkToken) {
                $validator = Validator::make($request->all(), [
                    'name' => 'max:127',
                    'birthday' => 'date'
                ]);
                if ($validator->fails()) {
                    return response()->json([
                        'status' => 0,
                        'error' => $validator->errors()], 401);
                }
                unset($data['token']);
                if (!empty($data['file'])) {
                    $extension = $data['file']->getClientOriginalExtension(); // getting image extension
                    $filename = time() . '.' . $extension;
                    $data['file']->move('images/users/', $filename);
                    $data['picture_name'] = $filename;
                    unset($data['file']);
                }
                if ($this->user->where('id', $id)->update($data)) {
                    return response()->json(['status' => 1]);
                }
                return response()->json(['status' => 0]);
            }
            return response()->json(['status' => 0, 'error' => 'Token not found or expired']);
        } catch (Exception $exception) {
            return response()->json(['status' => 0, 'error' => $exception->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function setMedicine($id)
    {
        try {
            $data = $this->request->all();
                if ($this->user->where('id', $id)->update($data)) {
                    return response()->json(['status' => 1]);
                }
                return response()->json(['status' => 0]);
        } catch (Exception $exception) {
            return response()->json(['status' => 0, 'error' => $exception->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $user = $this->user->find($id);
        $user->delete();

        return response()->json(['status' => Response::HTTP_OK]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $data = $this->request->all();
        if (empty($data['token'])) {
            throw new Exception('Token required', 1);
        }
        $checkToken = $this->checkToken($data['token']);
        $user = $this->user->find($id);
        return response()->json(['status' => 1, 'data' => $user]);
    }

    /**
     **
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        $data = $this->request->all();
        $user = User::where(['email' => $data['email'], 'password' => $data['password']])->first();
        if ($user) {
            $token['token'] = StringHelper::randomUnique(64);
            $token['user_id'] = $user->id;
            $token['delete_flg'] = 0;
            $this->auth->insert($token);
            $user['token'] = $token['token'];
            return response()->json(['data' => $user,
                'status' => 1]);
        } else {
            return response()->json(['data' => $user,
                'status' => 0]);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function logout()
    {
        $data = $this->request->all();
        $result = $this->checkToken($data['token']);
        if ($result) {
            $tokens = $this->auth->where('token', $data['token'])->first();
            $tokens->delete_flg = 1;
            $tokens->save();
            return response()->json(['status' => 1]);
        }
        return response()->json(['status' => 0]);
    }
}
