<?php

namespace App\Http\Controllers;

use App\Medicine;
use App\Message;
use App\Skincare;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class MessageController extends Controller
{
    protected $request;
    protected $user;

    /**
     *
     * @param Request $request
     * @param Product $user
     */
    public function __construct(Request $request, Message $user)
    {
        $this->request = $request;
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {
        $data = $this->request->all();
        $user = $this->user->where(function($query) use ($data){
                $query->where('sender', '<=', $data['user_id']);
                $query->orWhere('receiver', '>=', $data['user_id']);
            });
        if (!empty($id)) {
            if (!empty($data['is_before'])) {
                $user->where('id', '<', $id);
            } else {
                $user->where('id', '>', $id);
            }
        }
        $result = $user->get();
        return response()->json(['data' => $result,
            'status' => 1]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $data = $this->request->all();
            if (!empty($data['picture'])) {
                $extension = $data['picture']->getClientOriginalExtension(); // getting image extension
                $filename = time() . '.' . $extension;
                $data['picture']->move('images/medicines/', $filename);
                unset($data['picture']);
                $data['image'] = $filename;
            }
            $id = $this->user->insertGetId($data);
            $restful = $this->user->where('id', $id)->first();
            DB::commit();
            return response()->json(['status' => 1, 'data' => $restful]);
        } catch (Exception $exception) {
            DB::rollBack();
            return response()->json(['status' => 0, 'error' => $exception->getMessage()]);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $data = $this->request->all();
            $checkToken = $this->checkToken($data['token']);
            if ($checkToken) {
                $validator = Validator::make($request->all(), [
                    'name' => 'max:127',
                    'email' => 'required|email|max:127|unique:users,email,' . $id,
                    'password' => 'required',
                    'birthday' => 'date'
                ]);
                if ($validator->fails()) {
                    return response()->json([
                        'status' => 0,
                        'error' => $validator->errors()], 401);
                }
                unset($data['token']);
                if (!empty($data['file'])) {
                    $extension = $data['file']->getClientOriginalExtension(); // getting image extension
                    $filename = time() . '.' . $extension;
                    $data['file']->move('images/users/', $filename);
                    $data['picture'] = $filename;
                    unset($data['file']);
                }
                if ($this->user->where('id', $id)->update($data)) {
                    DB::commit();
                    return response()->json(['status' => 1]);
                }
                return response()->json(['status' => 0]);
            }
            return response()->json(['status' => 0, 'error' => 'Token not found or expired']);
        } catch (Exception $exception) {
            DB::rollBack();
            return response()->json(['status' => 0, 'error' => $exception->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $user = $this->user->find($id);
        $user->delete();

        return response()->json(['status' => 1]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function show($id)
    {
        try {
            $data = $this->request->all();
            $this->checkToken($data['token']);
            $symptomData = $this->user->where('id', $id)->with('symptomPictures')->first();
            return response()->json(['status' => 1, 'data' => $symptomData]);
        } catch (Exception $exception) {
            return response()->json(['status' => 0, 'error' => $exception->getMessage()]);
        }
    }
}
